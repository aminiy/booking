unit bookingscr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

resourcestring
  //Actions
  act_Search='Поиск';
  act_Personal='Личные данные';
  act_TeamEditor='Редактор команд';
  act_Events='Мероприятия';
  act_Table='Таблица';
  act_Form='Форма';
  act_Data='Данные';
  //Text Fields
  edt_SearchText='Введите Ник или ФАМИЛИЮ';
  //buttons
  btn_Search='Найти';
  btn_EditPerData='Данные';
  //DBGrird column and field titles
  tit_Emp='Работник';
  tit_EmpNo='№ работника';
  tit_Number='№';
  tit_Nick='Ник';
  tit_Surname='Фамилия';
  tit_Name='Имя';
  tit_Phone='Телефон';
  tit_Passport='№ Пасспорта';
  tit_Pasexd='Срок пасспорта';
  tit_Dob='Дата рождения';
  tit_Age='Возраст';
  tit_Country='Гражданство';
  tit_Groupe='Группа персонала';
  tit_Artist='Артист';
  tit_Office='Офисный работник';
  tit_Manager='Менеджер';
  tit_Cob='Страна рождения';
  tit_Arrival='Дата въезда';
  tit_StartWork='Дата начала';
  tit_ContDur='Срок контракта';
  tit_Email='Адрес эл.почты';
  tit_Skype='Адрес Skype';
  tit_EmerCont='Для экстренной связи';
  tit_isContry='Та же что и гражданство';
  tit_Date='Дата';
  tit_VisaType='Тып визы';
  tit_VisaNum='№ визы';
  tit_VisaExp='Срок визы';
  tit_Polreg='Рег. в Полиции';
  tit_SalPlan='Форма оплаты';
  tit_Sal='Зарплата';
  tit_Team='Команда';
  tit_HomeAd='Дом. адрес';
  tit_City='Город';
  tit_DepTime='Время вылета';
  tit_Fl='Номер рейса';
  tit_Airport='Аэропорт';
  tit_Price='Цена';
  tit_Det='Примечания';
  tit_ttype='Тип команды';
  tit_AddArt='Добавить Артиста';
  tit_DelArt='Удалить Артиста';
  tit_AddMan='Добавить менеджера';
  tit_DelMan='Удалить менеджера';
  tit_ARTeamMembers='Артисты члены команды';
  tit_EDARTeamMembers='Артисты к добавлению';
  tit_MenTeamMembers='Менеджеры члены команды';
  tit_EDMenTeamMembers='Менеджеры к добавлению';
  tit_SName='Наименование';
  tit_SPR='Справочник';
  tit_SPR_Pol='Справочник регистрации в полиции';
  tit_SPR_HA='Справочник домашних адресов';
  tit_SPR_SP='Справочник форм оплаты труда';
  tit_SPR_Cd='Справочник сроков контракта';


  //Hints
  hint_InvField='Поле заполнено не верно!';
  hint_NullField='Поле не заполнено!';
  hint_valField='Поле зополнено верно.';
  hint_UperCase='Только заглавные латинские буквы.';
  hint_Eng='Только латинские буквы';
  hint_Phone='11 цыфр, всегда начинается на 1.';
  hint_EmerPhone='Только цыфры.';
  hint_Arival='Дата начала работы'+Char(13)+'не может быть раньше даты въезда';
  hint_Visa='Дата истечения визы'+Char(13)+'не может быть раньше даты въезда';

  //Exception
  exc_WVVdate='Поле дата не должно быть пустым';
  exc_Viisa='Виза работника истекла или истекает в течение 7ми дней от указанной даты';
  exc_WD='У работника не заполны рабочие данные';
  exc_ART='В команде нет артиста.'+Char(13)+'Добавьте сначало артиста, потом менеджера';
  exc_V_Date='Работник имеет мероприятия на указанную дату';
  exc_V_Date_Af='Работник имеет мероприятия после указанной даты';
  exc_noTeam='На указанную дату состав команды не определен';
  exc_TeamMemberBusy='Однин или более членов команды имеют мероприятия на указанную дату';
  exc_TeamMemberVisaexp='Однин или более членов команды имеют просроченную визу на указанную дату';

implementation

end.

