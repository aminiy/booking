unit MainForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DB, FileUtil, Forms, Controls, Graphics, Dialogs,
  ComCtrls, Buttons, StdCtrls, DBGrids, DBCtrls, ActnList, IBCustomDataSet,
  IBTable, IBDatabase, IBQuery, LResources, DefaultTranslator, EditBtn,
  ExtCtrls, Menus, DBZVDateTimePicker, ZVDateTimePicker, LCLType;

type

  { TBookingMainForm }

  TBookingMainForm = class(TForm)
    ActFilter: TAction;
    ActSprs: TAction;
    ActAddAr: TAction;
    ActDelAr: TAction;
    ActAddMan: TAction;
    ActDelMan: TAction;
    ActTeamFilter: TAction;
    ActionList1: TActionList;
    ActPerDbGidrSheet: TAction;
    ActPerFormSheet: TAction;
    ActPeRWorkDataSheet: TAction;
    btnEditPer: TBitBtn;
    ConDurDS: TDatasource;
    DBEditName1: TDBEdit;
    DBEditName2: TDBEdit;
    DBEditName3: TDBEdit;
    DBEditName4: TDBEdit;
    DBNavigator7: TDBNavigator;
    DBNavigator8: TDBNavigator;
    DBNavigator9: TDBNavigator;
    LabSpName2: TLabel;
    LabSpName3: TLabel;
    LabSpName4: TLabel;
    SpGrid1: TDBGrid;
    DBNavigator6: TDBNavigator;
    EMPBusyDS: TDatasource;
    DBEditFl: TDBEdit;
    DBEditAir: TDBEdit;
    DBEdit14: TDBEdit;
    DBEditCity: TDBEdit;
    ERRGrid: TDBGrid;
    DBLookupTeams: TDBLookupComboBox;
    DBMemo1: TDBMemo;
    DBNavigator5: TDBNavigator;
    DBEventDate: TDBZVDateTimePicker;
    DBZDeparDatetime: TDBZVDateTimePicker;
    EMPBusyEMP: TLongintField;
    EMPBusyTEAM: TLongintField;
    EMPBusyVISA_DATE: TDateField;
    EventdDS: TDatasource;
    ArtistGrid: TDBGrid;
    EmpsStateSheet: TTabSheet;
    EMPBusy: TIBDataSet;
    EVENT_CHECK: TIBDataSet;
    PolGroupBox: TGroupBox;
    SalPlGroupBox: TGroupBox;
    HaGroupBox: TGroupBox;
    ContDurGroupBox: TGroupBox;
    LabSpName1: TLabel;
    Panel3: TPanel;
    Panel5: TPanel;
    SpGrid2: TDBGrid;
    SpGrid3: TDBGrid;
    SpGrid4: TDBGrid;
    StringField7: TStringField;
    StringField8: TStringField;
    SprsSheet: TTabSheet;
    TeammGird: TDBGrid;
    AddArtistdGrid: TDBGrid;
    AddTeammGird: TDBGrid;
    EvtGrid: TDBGrid;
    EMPSAREDIT: TIBDataSet;
    EMPSAREMP: TIBStringField;
    EMPSAREMP1: TIBStringField;
    EMPSARID: TLongintField;
    EMPSARID1: TLongintField;
    EMPSDSMAN: TDatasource;
    EMPSMANEDIT: TIBDataSet;
    EMPSMANEMP1: TIBStringField;
    EMPSMANID1: TLongintField;
    EventsTabAIRPORT: TIBStringField;
    EventsTabCITY: TIBStringField;
    EventsTabDEPARTURE_TIME: TDateTimeField;
    EventsTabDETAILS: TMemoField;
    EventsTabFLIGHT: TIBStringField;
    EventsTabID: TLongintField;
    EventsTabPRICE: TIBBCDField;
    EventsTabTEAMS_ID: TLongintField;
    EventsTabV_DATE: TDateField;
    EventSheet: TTabSheet;
    EventsTab: TIBTable;
    LabTeam1: TLabel;
    LabPrice: TLabel;
    LabFl: TLabel;
    LabAirport: TLabel;
    LabDet: TLabel;
    labvdate1: TLabel;
    LabCity: TLabel;
    LabDtime: TLabel;
    StringField6: TStringField;
    TEAM_CHECK: TIBDataSet;
    LabTeam3: TLabel;
    Labvdate2: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    StringField4: TStringField;
    StringField5: TStringField;
    TeamsList: TDBLookupComboBox;
    DSARTARTISTS: TLongintField;
    DSARTID: TLongintField;
    DSARTTEAMS_ID: TLongintField;
    DSARTV_DATE: TDateField;
    DSMANDS: TDatasource;
    DSARDS: TDatasource;
    DSMAN: TIBDataSet;
    DSART: TIBDataSet;
    DSMANID: TLongintField;
    DSMANMANAGERS: TLongintField;
    DSMANTEAMS_ID: TLongintField;
    DSMANV_DATE: TDateField;
    TeamFilterDate: TZVDateTimePicker;
    EMPSDAR: TDatasource;
    DBComboBox1: TDBComboBox;
    DBEdit6: TDBEdit;
    TeamGrid: TDBGrid;
    DBNavigator4: TDBNavigator;
    EMPSAR: TIBDataSet;
    LabTeam2: TLabel;
    LabTeamtype: TLabel;
    Panel2: TPanel;
    Panel4: TPanel;
    ToolBar10: TToolBar;
    ToolBar11: TToolBar;
    ToolBar12: TToolBar;
    ToolBar6: TToolBar;
    ToolBar7: TToolBar;
    ToolBar8: TToolBar;
    ToolBar9: TToolBar;
    VisaType: TDBComboBox;
    DBESal: TDBEdit;
    VisaNum: TDBEdit;
    DBLpolice: TDBLookupComboBox;
    DBLSalPlan: TDBLookupComboBox;
    DBLHomeA: TDBLookupComboBox;
    DBLookupComboBox5: TDBLookupComboBox;
    DBNavigator2: TDBNavigator;
    DBNavigator3: TDBNavigator;
    DBSearchGrid: TDBGrid;
    DBZV_Date: TDBZVDateTimePicker;
    DBZVisadate: TDBZVDateTimePicker;
    FindDataSetAGE: TLargeintField;
    FindDataSetARRIVAL_IN_CHINA: TDateField;
    FindDataSetARTISTS: TSmallintField;
    FindDataSetCOB: TIBStringField;
    FindDataSetCOB_IS_CONTRY: TSmallintField;
    FindDataSetCONTRACT_DURATION: TLongintField;
    FindDataSetCOUNTRY: TIBStringField;
    FindDataSetDOB: TDateField;
    FindDataSetEMAIL: TIBStringField;
    FindDataSetEMERGENCY_CONTACT: TIBStringField;
    FindDataSetEMP_NO: TIBStringField;
    FindDataSetID: TLongintField;
    FindDataSetMANAGERS: TSmallintField;
    FindDataSetNAME: TIBStringField;
    FindDataSetNICKNAME: TIBStringField;
    FindDataSetOFFICE: TSmallintField;
    FindDataSetPASSPORT: TIBStringField;
    FindDataSetPASSPORT_EXPIRATION_DATE: TDateField;
    FindDataSetPHONE: TIBStringField;
    FindDataSetSKYPE: TIBStringField;
    FindDataSetSTART_WORKING: TDateField;
    FindDataSetSURNAME: TIBStringField;
    labvdate: TLabel;
    Labvisatype: TLabel;
    Labvisanum: TLabel;
    Labevisaexp: TLabel;
    Labpolreg: TLabel;
    Labsalplan: TLabel;
    Labsal: TLabel;
    Labteam: TLabel;
    Labhomead: TLabel;
    Panel1: TPanel;
    PRSDS: TDatasource;
    SearchBtn: TBitBtn;
    SearchEdit: TEdit;
    SPSDS: TDatasource;
    HASDS: TDatasource;
    PRS: TStringField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    TMSDS: TDatasource;
    TM_STab: TIBTable;
    PR_Stab: TIBTable;
    SP_Stab: TIBTable;
    HA_STab: TIBTable;
    ToolBar4: TToolBar;
    ToolBar5: TToolBar;
    ToolButtonFilter: TToolButton;
    ToolButtonSPRS: TToolButton;
    WDDs: TDatasource;
    DBChOffice: TDBCheckBox;
    DBChManagers: TDBCheckBox;
    DBChArtits: TDBCheckBox;
    DBCheccobisconry: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBGEmp: TDBGrid;
    DBGridWDv: TDBGrid;
    DBLookupComboBox1: TDBLookupComboBox;
    DBNavigator1: TDBNavigator;
    DBText1: TDBText;
    DBText3: TDBText;
    DBZVDateTimePicker1: TDBZVDateTimePicker;
    DBZVDateTimePicker2: TDBZVDateTimePicker;
    DBZVArrival: TDBZVDateTimePicker;
    DBZStartWorking: TDBZVDateTimePicker;
    EmpsDS: TDatasource;
    EmpsTab: TIBTable;
    EmpsTabAGE: TLargeintField;
    EmpsTabARRIVAL_IN_CHINA: TDateField;
    EmpsTabARTISTS: TSmallintField;
    EmpsTabCOB: TIBStringField;
    EmpsTabCOB_IS_CONTRY: TSmallintField;
    EmpsTabCONTRACT_DURATION: TLongintField;
    EmpsTabCOUNTRY: TIBStringField;
    EmpsTabDOB: TDateField;
    EmpsTabEMAIL: TIBStringField;
    EmpsTabEMERGENCY_CONTACT: TIBStringField;
    EmpsTabEMP_NO: TIBStringField;
    EmpsTabID: TLongintField;
    EmpsTabMANAGERS: TSmallintField;
    EmpsTabNAME: TIBStringField;
    EmpsTabNICKNAME: TIBStringField;
    EmpsTabOFFICE: TSmallintField;
    EmpsTabPASSPORT: TIBStringField;
    EmpsTabPASSPORT_EXPIRATION_DATE: TDateField;
    EmpsTabPHONE: TIBStringField;
    EmpsTabSKYPE: TIBStringField;
    EmpsTabSTART_WORKING: TDateField;
    EmpsTabSURNAME: TIBStringField;
    empGroupBox: TGroupBox;
    ConDurT: TIBTable;
    WDTab: TIBTable;
    WDTabState: TIBTable;
    WDTabStateEMPLOYEES_ID: TLongintField;
    WDTabStateHOME_ADDRESS_ID: TLongintField;
    WDTabStatePOLICE_REGISTRATION_ID: TLongintField;
    WDTabStateSALARY: TIBBCDField;
    WDTabStateSALARY_PLAN_ID: TLongintField;
    WDTabStateSTATE: TSmallintField;
    WDTabStateTEAM_ID: TLongintField;
    WDTabStateVISA_EXPIRATION_DATE: TDateField;
    WDTabStateVISA_NUMBER: TIBStringField;
    WDTabStateVISA_TYPE: TIBStringField;
    WDTabStateV_DATE: TDateField;
    WDTabEMPLOYEES_ID: TLongintField;
    WDTabHOME_ADDRESS_ID: TLongintField;
    WDTabPOLICE_REGISTRATION_ID: TLongintField;
    WDTabSALARY: TIBBCDField;
    WDTabSALARY_PLAN_ID: TLongintField;
    WDTabTEAM_ID: TLongintField;
    WDTabVISA_EXPIRATION_DATE: TDateField;
    WDTabVISA_NUMBER: TIBStringField;
    WDTabVISA_TYPE: TIBStringField;
    WDTabV_DATE: TDateField;
    ImageList2: TImageList;
    LabDob: TLabel;
    LabAge: TLabel;
    LabCountr: TLabel;
    LabCob: TLabel;
    LabArinCn: TLabel;
    LabEmpno: TLabel;
    LabStartwor: TLabel;
    LabContdur: TLabel;
    Labemail: TLabel;
    LabSkype: TLabel;
    LabEmergcont: TLabel;
    LabPassexd: TLabel;
    LabPassport: TLabel;
    LabNick: TLabel;
    LabSurname: TLabel;
    LabName: TLabel;
    LabPhone: TLabel;
    PCP: TPageControl;
    SearchAction: TAction;
    PersonalAction: TAction;
    EditTeamAction: TAction;
    EventsAction: TAction;
    BookingActionList: TActionList;
    FindDS: TDatasource;
    IBDBc: TIBDatabase;
    FindDataSet: TIBDataSet;
    IBTran: TIBTransaction;
    ImageList1: TImageList;
    BookingPageControl: TPageControl;
    PersonalSheet: TTabSheet;
    TabSPersGird: TTabSheet;
    TabSPerForm: TTabSheet;
    TeamSheet: TTabSheet;
    SearchSheet: TTabSheet;
    ToolBar1: TToolBar;
    ToolBar2: TToolBar;
    ToolBar3: TToolBar;
    ToolButpersheet: TToolButton;
    ToolButform: TToolButton;
    ToolButsearch: TToolButton;
    ToolButtonPersonal: TToolButton;
    ToolButtonTeamEdt: TToolButton;
    ToolButtonEvents: TToolButton;
    procedure ActAddManExecute(Sender: TObject);
    procedure ActDelArExecute(Sender: TObject);
    procedure ActDelManExecute(Sender: TObject);
    procedure ActFilterExecute(Sender: TObject);
    procedure ActSprsExecute(Sender: TObject);
    procedure ActAddArExecute(Sender: TObject);
    procedure ActPerDbGidrSheetExecute(Sender: TObject);
    procedure ActPerFormSheetExecute(Sender: TObject);
    procedure ActPeRWorkDataSheetExecute(Sender: TObject);
    procedure ActTeamFilterExecute(Sender: TObject);
    procedure AddArtistdGridUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char
      );
    procedure btnEditPerClick(Sender: TObject);
    procedure DBCheccobisconryChange(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBZStartWorkingExit(Sender: TObject);
    procedure DBZV_DateExit(Sender: TObject);
    procedure EditTeamActionExecute(Sender: TObject);
    procedure EMPBusyNewRecord(DataSet: TDataSet);
    procedure EmpsTabAfterPost(DataSet: TDataSet);
    procedure EmpsTabAfterScroll(DataSet: TDataSet);
    procedure EmpsTabBeforePost(DataSet: TDataSet);
    procedure EmpsTabNewRecord(DataSet: TDataSet);
    procedure ERRGridDblClick(Sender: TObject);
    procedure EventsActionExecute(Sender: TObject);
    procedure EventSheetShow(Sender: TObject);
    procedure EventsTabAfterPost(DataSet: TDataSet);
    procedure EventsTabAfterScroll(DataSet: TDataSet);
    procedure EventsTabBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure PersonalSheetShow(Sender: TObject);
    procedure SearchActionExecute(Sender: TObject);
    procedure PersonalActionExecute(Sender: TObject);
    procedure SearchBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function CheckRegEx(Sender: TDBEdit; expr: string; desc: string): boolean;
    function CheckDates(): boolean;
    function CheckWDTab(): boolean;
    function CheckEvents(): boolean;
    procedure SearchSheetShow(Sender: TObject);
    procedure SprsSheetShow(Sender: TObject);
    procedure TabSPerFormShow(Sender: TObject);
    procedure TabSPersGirdShow(Sender: TObject);
    procedure TeamFilterDateChange(Sender: TObject);
    procedure TeamSheetShow(Sender: TObject);
    procedure TM_STabAfterPost(DataSet: TDataSet);
    procedure TM_STabBeforePost(DataSet: TDataSet);
    procedure VisaNumExit(Sender: TObject);
    procedure WDTabAfterPost(DataSet: TDataSet);
    procedure WDTabAfterScroll(DataSet: TDataSet);
    procedure WDTabBeforePost(DataSet: TDataSet);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  BookingMainForm: TBookingMainForm;
  old_team:integer;

implementation

{$R *.lfm}

uses bookingscr, regexpr;

{ TBookingMainForm }

procedure TBookingMainForm.FormShow(Sender: TObject);
begin
  HA_STab.Open;
  PR_Stab.Open;
  SP_Stab.Open;
  TM_STab.Open;
  ConDurT.Open;
  EmpsTab.Open;
  WDTab.Open;
  WDTabState.Open;
  EMPSAR.Open;
  EventsTab.Open;

  BookingPageControl.ShowTabs := False;
  PCP.ShowTabs := False;
  PersonalSheet.Show;
  SearchSheet.Show;
  SearchEdit.SetFocus;
end;

function TBookingMainForm.CheckRegEx(Sender: TDBEdit; expr: string;
  desc: string): boolean;
var
  RegexObj: TRegExpr;
begin
  Result:=True;
  Sender.Color := clWhite;
  RegexObj := TRegExpr.Create;
  RegexObj.Expression := expr;
  if RegexObj.Exec(Sender.Text) then
  begin
    Sender.Color := $00DDFFDD;
    Sender.Hint := hint_valField;
    Result := True;
  end
  else
  begin
    Sender.Color := $00DDDDFF;
    Sender.Hint := hint_InvField + chr(13) + desc;
    Result := False;
  end;
  RegexObj.Free;

end;

function TBookingMainForm.CheckEvents():Boolean;
begin
Result:=True;
if DBEventDate.DateIsNull then
   begin
   Result:=False;
   DBEventDate.Color := $00DDDDFF;
   DBEventDate.Hint:=hint_NullField;
   end
   else
   begin
   DBEventDate.Color := $00DDFFDD;
   DBEventDate.Hint := hint_valField;
   end;

if DBZDeparDatetime.DateIsNull then
   begin
   Result:=False;
   DBZDeparDatetime.Color := $00DDDDFF;
   DBZDeparDatetime.Hint:=hint_NullField;
   end
   else
   begin
   DBZDeparDatetime.Color := $00DDFFDD;
   DBZDeparDatetime.Hint := hint_valField;
   end;
if DBLookupTeams.KeyValue<0 then
   begin
   Result:=False;
   DBLookupTeams.Color := $00DDDDFF;
   DBLookupTeams.Hint:=hint_NullField;
   end
   else
   begin
   DBLookupTeams.Color := $00DDFFDD;
   DBLookupTeams.Hint := hint_valField;
   end;

Result:=CheckRegEx(DBEditCity, '^.+$', hint_NullField);
Result:=CheckRegEx(DBEditFl, '^.+$', hint_NullField);
Result:=CheckRegEx(DBEditAir, '^.+$', hint_NullField);

end;

procedure TBookingMainForm.SearchSheetShow(Sender: TObject);
begin
  ToolButsearch.Down:=True;
end;

procedure TBookingMainForm.SprsSheetShow(Sender: TObject);
begin
  ToolButtonSPRS.Down:=True;
end;

procedure TBookingMainForm.TabSPerFormShow(Sender: TObject);
begin
    ToolButform.Down:=True;
end;

procedure TBookingMainForm.TabSPersGirdShow(Sender: TObject);
begin
    ToolButpersheet.Down:=True;
end;

function TBookingMainForm.CheckDates(): boolean;
begin
  if EmpsTab.FieldByName('ARRIVAL_IN_CHINA').Value >=
    EmpsTab.FieldByName('START_WORKING').Value then
    begin
    DBZStartWorking.Color := $00DDDDFF;
    DBZStartWorking.Hint:=hint_Arival;
    Result := False;
    end
  else
  begin
    DBZStartWorking.Color := $00DDFFDD;
    DBZStartWorking.Hint:=hint_valField;
    Result:=True;
  end;

  DBZVArrival.Color:=DBZStartWorking.Color;
  DBZVArrival.Hint:=DBZStartWorking.Hint;

  if (WDTabState.FieldByName('VISA_EXPIRATION_DATE').AsString<>'') and
     (EmpsTab.FieldByName('ARRIVAL_IN_CHINA').Value>
     WDTabState.FieldByName('VISA_EXPIRATION_DATE').AsString) then
       begin
            DBZVisadate.Color:= $00DDDDFF;
            DBZVisadate.Hint:=hint_Visa;
            DBZVArrival.Hint:=DBZVisadate.Hint;
            DBZVArrival.Color:=DBZVisadate.Color;
            Result:=False;
       end;
end;

function TBookingMainForm.CheckWDTab():Boolean;
var visa:String;
begin
  Result:=True;

  Result:=CheckRegEx(VisaNum, '^.+$', hint_NullField);
  if DBZV_Date.DateIsNull then
       begin
       Result:=false;
            DBZV_Date.Color:= $00DDDDFF;
            DBZV_Date.Hint:=hint_NullField;
       end
       else
       begin
            DBZV_Date.Color:= $00DDFFDD;
            DBZV_Date.Hint:=hint_valField;
       end;

  if DBZVisadate.DateIsNull then
       begin
            Result:=false;
            DBZVisadate.Color:= $00DDDDFF;
            DBZVisadate.Hint:=hint_Visa + Char(13) +hint_NullField;
       end
       else
       begin
            DBZVisadate.Color:= $00DDFFDD;
            DBZVisadate.Hint:=hint_valField;
       end;

  if EmpsTab.FieldByName('ARRIVAL_IN_CHINA').Value>
     WDTab.FieldByName('VISA_EXPIRATION_DATE').Value then
       begin
            Result:=false;
            DBZVisadate.Color:= $00DDDDFF;
            if WDTab.FieldByName('VISA_EXPIRATION_DATE').AsString='' then
               DBZVisadate.Hint:=hint_Visa + Char(13) +hint_NullField
            else
               DBZVisadate.Hint:=hint_Visa;
       end
       else
       begin
            DBZVisadate.Color:= $00DDFFDD;
            DBZVisadate.Hint:=hint_valField;
       end;

  if (WDTab.FieldByName('VISA_TYPE').AsString='') then
       begin
            Result:=false;
            VisaType.Color:= $00DDDDFF;
            VisaType.Hint:=hint_NullField
       end
       else
       begin
            VisaType.Color:= $00DDFFDD;
            VisaType.Hint:=hint_valField;
       end;

  if (WDTab.FieldByName('POLICE_REGISTRATION_ID').AsString='') then
       begin
            Result:=false;
            DBLpolice.Color:= $00DDDDFF;
            DBLpolice.Hint:=hint_invField
       end
       else
       begin
            DBLpolice.Color:= $00DDFFDD;
            DBLpolice.Hint:=hint_valField;
       end;

  if (WDTab.FieldByName('SALARY_PLAN_ID').AsString='') then
       begin
            Result:=false;
            DBLSalPlan.Color:= $00DDDDFF;
            DBLSalPlan.Hint:=hint_invField
       end
       else
       begin
            DBLSalPlan.Color:= $00DDFFDD;
            DBLSalPlan.Hint:=hint_valField;
       end;

  if (WDTab.FieldByName('SALARY').AsFloat=0) then
       begin
            Result:=false;
            DBESal.Color:= $00DDDDFF;
            DBESal.Hint:=hint_invField
       end
       else
       begin
            DBESal.Color:= $00DDFFDD;
            DBESal.Hint:=hint_valField;
       end;

  if (WDTab.FieldByName('HOME_ADDRESS_ID').AsString='') then
       begin
            Result:=false;
            DBLHomeA.Color:= $00DDDDFF;
            DBLHomeA.Hint:=hint_invField
       end
       else
       begin
            DBLHomeA.Color:= $00DDFFDD;
            DBLHomeA.Hint:=hint_valField;
       end;

  if (WDTab.FieldByName('EMPLOYEES_ID').AsString='') then
       Result:=False;

end;

procedure TBookingMainForm.TeamFilterDateChange(Sender: TObject);
begin

end;

procedure TBookingMainForm.TeamSheetShow(Sender: TObject);
begin
  ToolButtonTeamEdt.Down:=True;
end;

procedure TBookingMainForm.TM_STabAfterPost(DataSet: TDataSet);
begin
  IBTran.CommitRetaining;
  HA_STab.Refresh;
  PR_Stab.Refresh;
  SP_Stab.Refresh;
  TM_STab.Refresh;
  ConDurT.Refresh;
  EmpsTab.Refresh;
  WDTab.Refresh;
  WDTabState.Refresh;
  EMPSAR.Refresh;
  EventsTab.Refresh;
end;

procedure TBookingMainForm.TM_STabBeforePost(DataSet: TDataSet);
begin
    if not CheckRegEx(DBEdit6, '^.+$', hint_NullField) then Abort;
end;

procedure TBookingMainForm.VisaNumExit(Sender: TObject);
begin
       CheckRegEx(VisaNum, '^.+$', hint_NullField);
end;

procedure TBookingMainForm.WDTabAfterPost(DataSet: TDataSet);
begin
  IBTran.CommitRetaining;
  WDTab.Refresh;
  WDTabState.Refresh;
end;

procedure TBookingMainForm.WDTabAfterScroll(DataSet: TDataSet);
begin
  CheckWDTab();
end;

procedure TBookingMainForm.WDTabBeforePost(DataSet: TDataSet);
begin
  if not CheckWDTab() then
    Abort;
end;

procedure TBookingMainForm.SearchBtnClick(Sender: TObject);
begin
  FindDataSet.Close;
  FindDataSet.Params.ByName('search').AsString := '%' + SearchEdit.Text + '%';
  FindDataSet.Params.ByName('search_no').AsString := '%' + SearchEdit.Text + '%';
  FindDataSet.Open;
end;

procedure TBookingMainForm.SearchActionExecute(Sender: TObject);
begin
  SearchSheet.Show;
  SearchEdit.SetFocus;
end;

procedure TBookingMainForm.FormCreate(Sender: TObject);
begin
  Application.HintPause := 10;

  SearchEdit.Text := edt_SearchText;
  SearchEdit.Hint := edt_SearchText;

  SearchBtn.Caption := btn_Search;
  SearchBtn.Hint := btn_Search;
  btnEditPer.Caption := btn_EditPerData;
  btnEditPer.Hint := btn_EditPerData;

  SearchAction.Caption := act_Search;
  SearchAction.Hint := act_Search;
  PersonalAction.Caption := act_Personal;
  PersonalAction.Hint := act_Personal;
  EditTeamAction.Caption := act_TeamEditor;
  EditTeamAction.Hint := act_TeamEditor;
  EventsAction.Caption := act_Events;
  EventsAction.Hint := act_Events;
  ActPerDbGidrSheet.Caption := act_Table;
  ActPerDbGidrSheet.Hint := act_Table;
  ActPerFormSheet.Caption := act_Form;
  ActPerFormSheet.Hint := act_Form;
  ActPeRWorkDataSheet.Caption := act_Data;
  ActPeRWorkDataSheet.Hint := act_Data;
  ActAddAr.Caption:=tit_AddArt;
  ActAddAr.Hint:=tit_AddArt;
  ActDelAr.Caption:=tit_DelArt;
  ActDelAr.Hint:=tit_DelArt;
  ActAddMan.Caption:=tit_AddMan;
  ActAddMan.Hint:=tit_AddMan;
  ActDelMan.Caption:=tit_DelMan;
  ActDelMan.Hint:=tit_DelMan;

  LabEmpno.Caption := tit_EmpNo;
  LabNick.Caption := tit_Nick;
  LabName.Caption := tit_Name;
  LabSurname.Caption := tit_Surname;
  LabPhone.Caption := tit_Phone;
  Labemail.Caption := tit_Email;
  DBChOffice.Caption := tit_Office;
  DBChManagers.Caption := tit_Manager;
  DBChArtits.Caption := tit_Artist;
  LabCountr.Caption := tit_Country;
  LabSkype.Caption := tit_Skype;
  LabPassport.Caption := tit_Passport;
  LabPassexd.Caption := tit_Pasexd;
  LabDob.Caption := tit_Dob;
  LabAge.Caption := tit_Age;
  LabCountr.Caption := tit_Country;
  empGroupBox.Caption := tit_Groupe;
  LabCob.Caption := tit_Cob;
  DBCheccobisconry.Caption := tit_isContry;
  LabArinCn.Caption := tit_Arrival;
  LabStartwor.Caption := tit_StartWork;
  LabContdur.Caption := tit_ContDur;
  LabEmergcont.Caption := tit_EmerCont;
  Labvdate.Caption:=tit_Date;
  Labvisatype.Caption:=tit_VisaType;
  Labvisanum.Caption:=tit_VisaNum;
  Labevisaexp.Caption:=tit_VisaExp;
  Labpolreg.Caption:=tit_Polreg;
  Labsalplan.Caption:=tit_SalPlan;
  Labsal.Caption:=tit_Sal;
  Labteam.Caption:=tit_Team;
  Labhomead.Caption:=tit_HomeAd;
  Labvdate1.Caption:=tit_Date;
  Labteam1.Caption:=tit_Team;
  LabCity.Caption:=tit_City;
  LabDtime.Caption:=tit_DepTime;
  LabFl.Caption:=tit_Fl;
  LabAirport.Caption:=tit_Airport;
  LabPrice.Caption:=tit_Price;
  LabDet.Caption:=tit_Det;
  Labvdate2.Caption:=tit_Date;
  Labteam2.Caption:=tit_Team;
  Labteam3.Caption:=tit_Team;
  LabTeamtype.Caption:=tit_ttype;
  LabSpName1.Caption:=tit_SName;
  LabSpName2.Caption:=tit_SName;
  LabSpName3.Caption:=tit_SName;
  LabSpName4.Caption:=tit_SName;


  PolGroupBox.Caption:=tit_SPR_Pol;
  SalPlGroupBox.Caption:=tit_SPR_SP;
  HaGroupBox.Caption:=tit_SPR_HA;
  ContDurGroupBox.Caption:=tit_SPR_Cd;

  ArtistGrid.Columns.Items[0].Title.Caption:=tit_ARTeamMembers;
  AddArtistdGrid.Columns.Items[0].Title.Caption:=tit_EDARTeamMembers;
  TeammGird.Columns.Items[0].Title.Caption:=tit_MenTeamMembers;
  AddTeammGird.Columns.Items[0].Title.Caption:=tit_EDMenTeamMembers;



  SpGrid1.Columns.Items[1].Title.Caption:=tit_SName;
  SpGrid2.Columns.Items[1].Title.Caption:=tit_SName;
  SpGrid3.Columns.Items[1].Title.Caption:=tit_SName;
  SpGrid4.Columns.Items[1].Title.Caption:=tit_SName;
  SpGrid1.Columns.Items[0].Title.Caption:=tit_Number;
  SpGrid2.Columns.Items[0].Title.Caption:=tit_Number;
  SpGrid3.Columns.Items[0].Title.Caption:=tit_Number;
  SpGrid4.Columns.Items[0].Title.Caption:=tit_Number;


  DBGridWDv.Columns.Items[0].Title.Caption:=tit_Date;
  DBGridWDv.Columns.Items[1].Title.Caption:=tit_VisaType;
  DBGridWDv.Columns.Items[2].Title.Caption:=tit_VisaNum;
  DBGridWDv.Columns.Items[3].Title.Caption:=tit_VisaExp;
  DBGridWDv.Columns.Items[4].Title.Caption:=tit_Sal;
  DBGridWDv.Columns.Items[5].Title.Caption:=tit_SalPlan;
  DBGridWDv.Columns.Items[6].Title.Caption:=tit_Polreg;
  DBGridWDv.Columns.Items[7].Title.Caption:=tit_HomeAd;
  DBGridWDv.Columns.Items[8].Title.Caption:=tit_Team;

  TeamGrid.Columns.Items[0].Title.Caption:=tit_Number;
  TeamGrid.Columns.Items[1].Title.Caption:=tit_Team;
  TeamGrid.Columns.Items[2].Title.Caption:=tit_ttype;

  EvtGrid.Columns.Items[0].Title.Caption:=tit_Date;
  EvtGrid.Columns.Items[1].Title.Caption:=tit_City;
  EvtGrid.Columns.Items[2].Title.Caption:=tit_DepTime;
  EvtGrid.Columns.Items[3].Title.Caption:=tit_Fl;
  EvtGrid.Columns.Items[4].Title.Caption:=tit_Airport;
  EvtGrid.Columns.Items[5].Title.Caption:=tit_Team;
  EvtGrid.Columns.Items[6].Title.Caption:=tit_Price   ;


  DBGEmp.Columns.Items[0].Title.Caption := tit_Number;
  DBGEmp.Columns.Items[1].Title.Caption := tit_EmpNo;
  DBGEmp.Columns.Items[2].Title.Caption := tit_Nick;
  DBGEmp.Columns.Items[3].Title.Caption := tit_Name;
  DBGEmp.Columns.Items[4].Title.Caption := tit_Surname;
  DBGEmp.Columns.Items[5].Title.Caption := tit_Phone;
  DBGEmp.Columns.Items[6].Title.Caption := tit_Skype;
  DBGEmp.Columns.Items[7].Title.Caption := tit_Email;
  DBGEmp.Columns.Items[8].Title.Caption := tit_Office;
  DBGEmp.Columns.Items[9].Title.Caption := tit_Manager;
  DBGEmp.Columns.Items[10].Title.Caption := tit_Artist;
  DBGEmp.Columns.Items[11].Title.Caption := tit_Country;

  DBSearchGrid.Columns.Items[0].Title.Caption := tit_Number;
  DBSearchGrid.Columns.Items[1].Title.Caption := tit_Nick;
  DBSearchGrid.Columns.Items[2].Title.Caption := tit_Surname;
  DBSearchGrid.Columns.Items[3].Title.Caption := tit_Name;
  DBSearchGrid.Columns.Items[4].Title.Caption := tit_Age;

  ERRGrid.Columns.Items[0].Title.Caption:=tit_Emp;
  ERRGrid.Columns.Items[1].Title.Caption:=tit_Team;
  ERRGrid.Columns.Items[2].Title.Caption:=tit_VisaExp;
end;

procedure TBookingMainForm.PersonalSheetShow(Sender: TObject);
begin
  ToolButtonPersonal.Down:=True;
end;

procedure TBookingMainForm.DBZStartWorkingExit(Sender: TObject);
begin
   CheckDates();
end;

procedure TBookingMainForm.DBZV_DateExit(Sender: TObject);
begin

end;

procedure TBookingMainForm.EditTeamActionExecute(Sender: TObject);
begin
  TeamSheet.Show;
end;

procedure TBookingMainForm.EMPBusyNewRecord(DataSet: TDataSet);
begin
  EventsTab.FieldByName('ID').AsInteger:=0;
end;

procedure TBookingMainForm.EmpsTabAfterPost(DataSet: TDataSet);
VAR RecNo:Longint;
begin
  RecNo:=EmpsTab.RecNo;
  IBTran.CommitRetaining;
  EmpsTab.Refresh;
  EmpsTab.RecNo:=RecNo;
end;

procedure TBookingMainForm.EmpsTabAfterScroll(DataSet: TDataSet);
begin
  CheckRegEx(DBEdit2, '^[A-Z]*$', hint_UperCase);
  CheckRegEx(DBEdit3, '^[A-Z]*$', hint_UperCase);
  CheckRegEx(DBEdit4, '^1([0-9]{10})$', hint_Phone);
  CheckRegEx(DBEdit7, '^[a-z|A-Z]*$', hint_Eng);
  CheckRegEx(DBEdit11, '^[0-9]*$', hint_EmerPhone);
  CheckDates()
end;

procedure TBookingMainForm.EmpsTabBeforePost(DataSet: TDataSet);
begin
  DBZStartWorkingExit(DataSet);
  if not (
  CheckRegEx(DBEdit2, '^[A-Z]*$', hint_UperCase) and
  CheckRegEx(DBEdit3, '^[A-Z]*$', hint_UperCase) and
  CheckRegEx(DBEdit4, '^1([0-9]{10})$', hint_Phone) and
  CheckRegEx(DBEdit7, '^[a-z|A-Z]*$', hint_Eng) and
  CheckRegEx(DBEdit11, '^[0-9]*$', hint_EmerPhone) and
  CheckDates()) then Abort;
end;

procedure TBookingMainForm.EmpsTabNewRecord(DataSet: TDataSet);
begin
EmpsTab.FieldByName('COB_IS_CONTRY').AsInteger:=0;
EmpsTab.FieldByName('ARTISTS').AsInteger:=0;
EmpsTab.FieldByName('MANAGERS').AsInteger:=0;
EmpsTab.FieldByName('OFFICE').AsInteger:=0;
end;

procedure TBookingMainForm.ERRGridDblClick(Sender: TObject);
begin
PersonalSheet.Show;
  TabSPersGird.Show;
  ToolButpersheet.Down := True;
  EmpsTab.Filter := 'ID=' + EMPBusy.FieldByName('EMP').AsString;
  if FindDataSet.RecordCount > 0 then
    EmpsTab.Filtered := True;
  EmpsTab.Open;
  ToolButtonFilter.Down:=True;
  ToolButtonFilter.Enabled:=True;
end;

procedure TBookingMainForm.EventsActionExecute(Sender: TObject);
begin
  EventSheet.Show;
end;

procedure TBookingMainForm.EventSheetShow(Sender: TObject);
begin
  ToolButtonEvents.Down:=True;
end;

procedure TBookingMainForm.EventsTabAfterPost(DataSet: TDataSet);
VAR RecNo:Longint;
begin
  RecNo:=EventsTab.RecNo;
  IBTran.CommitRetaining;
  EventsTab.Refresh;
  EventsTab.RecNo:=RecNo;
end;

procedure TBookingMainForm.EventsTabAfterScroll(DataSet: TDataSet);
begin

end;

procedure TBookingMainForm.EventsTabBeforePost(DataSet: TDataSet);
begin
  if not CheckEvents() then Abort
  else
  begin
       EVENT_CHECK.Close;
       EVENT_CHECK.ParamByName('V_DATE').AsDate:=EventsTab.FieldByName('V_DATE').AsDateTime;
       EVENT_CHECK.ParamByName('TEAMS_ID').AsInteger:=DBLookupTeams.KeyValue;
       EVENT_CHECK.ParamByName('EVENT_ID').AsVariant:=EventsTab.FieldByName('ID').AsVariant;
       EVENT_CHECK.Open;
       EMPBusy.Close;
       EMPBusy.ParamByName('V_DATE').AsDate:=EventsTab.FieldByName('V_DATE').AsDateTime;
       EMPBusy.ParamByName('TEAMS_ID').AsInteger:=DBLookupTeams.KeyValue;
       EMPBusy.ParamByName('EVENT_ID').AsVariant:=EventsTab.FieldByName('ID').AsVariant;
       EMPBusy.Open;

       ShowMessage(EVENT_CHECK.FieldByName('ERR_NO').AsString);

       CASE EVENT_CHECK.FieldByName('ERR_NO').AsInteger OF
       0:;
       1:BEGIN
         //Если нет состава команды за эту дату
            ShowMessage(exc_noTeam);
            TeamSheet.Show;
            TeamsList.KeyValue:=DBLookupTeams.KeyValue;
            TeamFilterDate.Date:=EventsTab.FieldByName('V_DATE').AsDateTime;
            Abort;
         END;

       2:begin
            ShowMessage(exc_TeamMemberBusy);
            EmpsStateSheet.Show;
            Abort;
         end;
       3:begin
            ShowMessage(exc_TeamMemberVisaexp);
            EmpsStateSheet.Show;
            Abort;
         end;

       end;
     END;

end;

procedure TBookingMainForm.ActPerDbGidrSheetExecute(Sender: TObject);
begin
  TabSPersGird.Show;

end;

procedure TBookingMainForm.ActFilterExecute(Sender: TObject);
begin
  if not (EmpsTab.Filtered) and (Length(EmpsTab.Filter)>0) then
    begin
         ToolButtonFilter.Down:=True;
         EmpsTab.Filtered:=True;
    end
    else
    begin
         ToolButtonFilter.Down:=False;
         EmpsTab.Filtered:=False;
    end;
end;

procedure TBookingMainForm.ActDelArExecute(Sender: TObject);
begin
  DSART.Delete;
  DSART.Close;
  DSART.Open;
  EMPSAREDIT.Close;
  EMPSAREDIT.Open;
end;

procedure TBookingMainForm.ActDelManExecute(Sender: TObject);
begin
  DSMAN.Delete;
  DSMAN.Close;
  DSMAN.Open;
  EMPSMANEDIT.Close;
  EMPSMANEDIT.Open;
end;

procedure TBookingMainForm.ActAddManExecute(Sender: TObject);
begin
  TEAM_CHECK.Close;
  TEAM_CHECK.ParamByName('V_DATE').AsDate:=TeamFilterDate.Date;
  TEAM_CHECK.ParamByName('TEAM_ID').AsInteger:=TeamsList.KeyValue;
  TEAM_CHECK.ParamByName('EMP_ID').AsInteger:=EMPSMANEDIT.FieldByName('ID').AsInteger;
  TEAM_CHECK.ParamByName('GROUPE').AsString:='M';
  TEAM_CHECK.Open;
  case TEAM_CHECK.FieldByName('ERR_NO').AsInteger of
  0:begin
         DSMAN.AppendRecord([Null, TeamFilterDate.Date, EMPSMANEDIT.FieldByName('ID').AsInteger,
         TeamsList.KeyValue]);
         IBTran.CommitRetaining;
         DSMAN.Close;
         DSMAN.Open;
         EMPSMANEDIT.Close;
         EMPSMANEDIT.Open;
  end;
  1:ShowMessage(exc_WD);
  2:ShowMessage(exc_Viisa);
  3:ShowMessage(exc_ART);
  4:ShowMessage(exc_V_Date);
  5:ShowMessage(exc_V_Date_Af);
  end;

end;

procedure TBookingMainForm.ActSprsExecute(Sender: TObject);
begin
  SprsSheet.Show;
end;

procedure TBookingMainForm.ActAddArExecute(Sender: TObject);
begin
  TEAM_CHECK.Close;
  TEAM_CHECK.ParamByName('V_DATE').AsDate:=TeamFilterDate.Date;
  TEAM_CHECK.ParamByName('TEAM_ID').AsInteger:=TeamsList.KeyValue;
  TEAM_CHECK.ParamByName('EMP_ID').AsInteger:=EMPSAREDIT.FieldByName('ID').AsInteger;
  TEAM_CHECK.ParamByName('GROUPE').AsString:='A';
  TEAM_CHECK.Open;

  case TEAM_CHECK.FieldByName('ERR_NO').AsInteger of
  0:begin
         DSART.AppendRecord([Null, TeamFilterDate.Date, EMPSAREDIT.FieldByName('ID').AsInteger,
         TeamsList.KeyValue]);
         IBTran.CommitRetaining;
         DSART.Close;
         DSART.Open;
         EMPSAREDIT.Close;
         EMPSAREDIT.Open;
    end;
  1:ShowMessage(exc_WD);
  2:ShowMessage(exc_Viisa);
  3:ShowMessage(exc_ART);
  4:ShowMessage(exc_V_Date);
  5:ShowMessage(exc_V_Date_Af);
  end;

end;

procedure TBookingMainForm.ActPerFormSheetExecute(Sender: TObject);
begin
  TabSPerForm.Show;
end;

procedure TBookingMainForm.ActPeRWorkDataSheetExecute(Sender: TObject);
begin
  SearchSheet.Show;
end;

procedure TBookingMainForm.ActTeamFilterExecute(Sender: TObject);
var Result:Boolean;
begin

    Result:=True;

    if TeamsList.KeyValue<0 then
       begin
            Result:=false;
            TeamsList.Color:= $00DDDDFF;
            TeamsList.Hint:=hint_invField
       end
       else
       begin
            TeamsList.Color:= $00DDFFDD;
            TeamsList.Hint:=hint_valField;
       end;

    if TeamFilterDate.DateIsNull then
       begin
            Result:=false;
            TeamFilterDate.Color:= $00DDDDFF;
            TeamFilterDate.Hint:=hint_invField
       end
       else
       begin
            TeamFilterDate.Color:= $00DDFFDD;
            TeamFilterDate.Hint:=hint_valField;
       end;
    if Result then
      begin
    DSART.Close;
    DSMAN.Close;
    EMPSAREDIT.Close;
    EMPSMANEDIT.Close;
    DSART.ParamByName('V_DATE').AsDate:=TeamFilterDate.Date;
    DSMAN.ParamByName('V_DATE').AsDate:=TeamFilterDate.Date;
    DSMAN.ParamByName('ID').AsInteger:=TeamsList.KeyValue;
    DSART.ParamByName('ID').AsInteger:=TeamsList.KeyValue;
    EMPSAREDIT.ParamByName('V_DATE').AsDate:=TeamFilterDate.Date;
    EMPSMANEDIT.ParamByName('V_DATE').AsDate:=TeamFilterDate.Date;
    EMPSAREDIT.ParamByName('ID').AsInteger:=TeamsList.KeyValue;
    EMPSMANEDIT.ParamByName('ID').AsInteger:=TeamsList.KeyValue;
    EMPSAR.Refresh;

    EMPSAREDIT.Open;
    EMPSMANEDIT.Open;
    DSART.Open;
    DSMAN.Open;

      end;


end;

procedure TBookingMainForm.AddArtistdGridUTF8KeyPress(Sender: TObject;
  var UTF8Key: TUTF8Char);
begin

end;

procedure TBookingMainForm.btnEditPerClick(Sender: TObject);
begin
  TabSPersGird.Show;
  EmpsTab.Filter := 'ID=' + FindDataSet.FieldByName('ID').AsString;
  if FindDataSet.RecordCount > 0 then
    EmpsTab.Filtered := True;
  EmpsTab.Open;
  ToolButtonFilter.Down:=True;
  ToolButtonFilter.Enabled:=True;
end;

procedure TBookingMainForm.DBCheccobisconryChange(Sender: TObject);
begin
  if DBCheccobisconry.Checked then
    DBEdit8.Enabled := False
  else
    DBEdit8.Enabled := True;
end;

procedure TBookingMainForm.DBEdit11Exit(Sender: TObject);
begin
  CheckRegEx(DBEdit11, '^[0-9]*$', hint_EmerPhone);
end;

procedure TBookingMainForm.DBEdit2Exit(Sender: TObject);
begin
  CheckRegEx(DBEdit2, '^[A-Z]*$', hint_UperCase);
end;

procedure TBookingMainForm.DBEdit3Exit(Sender: TObject);
begin
  CheckRegEx(DBEdit3, '^[A-Z]*$', hint_UperCase);
end;

procedure TBookingMainForm.DBEdit4Exit(Sender: TObject);
begin
  CheckRegEx(DBEdit4, '^1([0-9]{10})$', hint_Phone);
end;

procedure TBookingMainForm.DBEdit7Exit(Sender: TObject);
begin
  CheckRegEx(DBEdit7, '^[a-z|A-Z]*$', hint_Eng);
end;

procedure TBookingMainForm.PersonalActionExecute(Sender: TObject);
begin
  PersonalSheet.Show;
end;

end.

