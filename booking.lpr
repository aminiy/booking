program booking;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, MainForm, ibexpress, zvdatetimectrls, memdslaz, bookingscr, regexpr;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TBookingMainForm, BookingMainForm);
  Application.Run;
end.

